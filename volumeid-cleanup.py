# Script takes the input list of volumeid's and check if it has snapshot and then deletes the snapshot id. 

import boto3
import botocore

client = boto3.client('ec2',region_name='ap-southeast-2')


file=open('volumes.txt','r')
vList=file.read().splitlines()


#Describe volumes that are in an available state
volumes = client.describe_volumes(Filters=[{'Name': 'status', 'Values': ['available']}])


#Loop through volumes in an available state
for volume in volumes['Volumes']:

    vId = volume['VolumeId']
    if vId in vList:

        #Get the snapshots of this volume
        snapshots = client.describe_snapshots( Filters=[{'Name': 'volume-id', 'Values': [vId]}])

        if snapshots['Snapshots']:
            #If there are snapshot print the snapshot id and latest creation date 
            snapId=snapshots['Snapshots'][0]['SnapshotId'] 
            start=str(snapshots['Snapshots'][0]['StartTime'])
            creation="Creation:"

            print ("Volume:  " + vId + "\tSnapshot:  " + snapId + "\t" + creation + " "  + start)
            print("Deleting Snapshot", snapId)

            client.delete_snapshot(SnapshotId=snapId)
        else:
            #If there are no snapshots print just None
            snapId="None"
            start=""
            creation=""

            print ("Volume:  " + vId + "\tSnapshot:  " + snapId + "\t" + creation + " "  + start)

        try:
            client.delete_volume(VolumeId=vId)
            print("Volume Deleting ", vId)
        except botocore.exceptions.ClientError:
            message = "Volume id '{id}' not found"
            print (message.format(id = vId))

    


