# Script takes the input list of snapshotid's and check if it is associated with an ami and deregisters it and then deletes the snapshot id. 
# If not associated with ami, it deletes the snapshot id.


import boto3
import botocore
import numpy as np

client = boto3.client('ec2',region_name='ap-southeast-2')

file=open('snapshots.txt','r')

snaps=file.read().splitlines()

amiIds = []  #creating empty array for ami id
snapIds = [] #creating empty array for snap id

img_desc = client.describe_images(Owners=['self'])['Images']  #gets list of all ami's
for snap in img_desc:

    #looping to get list of all Block Devicc Mappings
    devices = snap['BlockDeviceMappings']

    #Loop to get list of all the SnapshotID within each device if the device type is 'Ebs'
    for device in devices:
        if 'Ebs' in device.keys():
            snapId = device.get('Ebs').get('SnapshotId')

            #If SnapshotId from the input text file matches to shanpshot id under each AMI, perform below.
            if snapId in snaps:
                #Gets all ImageId's for each of the above snapshot id's and appends them to array
                amiIds.append(snap.get('ImageId'))
                #Gets all ImageId's for each of the above snapshot id's and appends them to array
                snapIds.append(snapId)
                #print (snap.get('ImageId'), snapId)
            
#removes any dulicates from list
amiIds = list(set(amiIds))
snapIds = list(set(snapIds))

#Compares input provided snapshot id's with AMI associated snapshot id's and provides the difference
snapswithoutami = np.setdiff1d(snaps,snapIds)

#Deregisteres AMI's which are associated with provided snapshot id's.
for amiID in amiIds:
    print('Deregistering:',amiID)
    client.deregister_image(ImageId=amiID)

#Removes SnapshotId's which are linked with AMI's
for snpId in snapIds:
    print('Deleting:',snpId)
    client.delete_snapshot(SnapshotId=snpId)

#Removes SnapshotID's which are not linked with AMI's but provided in Snapshot List.
for snpIdnotinAmis in snapswithoutami:
    try:
        print('Deleting SnapshotId not in Ami:',snpIdnotinAmis)
        client.delete_snapshot(SnapshotId=snpIdnotinAmis)
    except botocore.exceptions.ClientError:
        message = "Snapshot id '{id}' not found"
        print (message.format(id = snpIdnotinAmis))