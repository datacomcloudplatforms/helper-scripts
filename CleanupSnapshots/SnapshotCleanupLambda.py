# Script takes the input list of snapshotid's and check if it is associated with an ami and deregisters it and then deletes the snapshot id. 
# If not associated with ami, it deletes the snapshot id.
# The code is updated to check whether AMI is being used in Launch Template or Autoscaling Groups
import boto3
import botocore
import os
import csv
from time import sleep

def lambda_handler(event, context):
    region = os.environ['AWS_REGION']
    account_id = context.invoked_function_arn.split(":")[4]

    key = os.environ['S3KeyName']
    bucket = os.environ['S3BucketName']
    s3_resource = boto3.resource('s3')
    s3_object = s3_resource.Object(bucket, key)

    amiIds = []  #creating empty array for ami id
    snapIds = [] #creating empty array for snap id
    SnapshotList = []

    get_ami_in_launch_configuration()
    #print('excludeAMIList:',excludedAMIs)

    client = boto3.client('ec2',region_name='ap-southeast-2')   
    data = s3_object.get()['Body'].read().decode('utf-8').splitlines()

    lines = csv.reader(data)
    headers = next(lines)
    print('headers: %s' %(headers))
    for line in lines:
        #print complete line
        print(line)
        #print index wise
        print(line[0], line[1])
        SnapshotList.append(line[1].strip())

    img_desc = client.describe_images(Owners=['self'])['Images']  #gets list of all ami's
    for snap in img_desc:

        #looping to get list of all Block Devicc Mappings
        devices = snap['BlockDeviceMappings']

        #Loop to get list of all the SnapshotID within each device if the device type is 'Ebs'
        for device in devices:
            if 'Ebs' in device.keys():
                snapId = device.get('Ebs').get('SnapshotId')

                #If SnapshotId from the input text file matches to shanpshot id under each AMI, perform below.
                if snapId in SnapshotList:
                    #Gets all ImageId's for each of the above snapshot id's and appends them to array
                    amiIds.append(snap.get('ImageId'))
                    #Gets all ImageId's for each of the above snapshot id's and appends them to array
                    if check_if_snapshot_associated_with_launch_configuration(snap.get('ImageId')) == False:
                        snapIds.append(snapId)
                        print("snap.get('ImageId')")
                        print(snap.get('ImageId'))

    #removes any dulicates from list
    amiIds = list(set(amiIds))
    snapIds = list(set(snapIds))
    excludedAMIIds = list(set(excludedAMIs))

    print('amiIds:',amiIds)
    print('snapIds:',snapIds)
    print('excludedAMIIds:',excludedAMIs)

    snapShList = list(set(SnapshotList))
    print('snapShList:',SnapshotList)

    #Compares input provided snapshot id's with AMI associated snapshot id's and provides the difference
    #snapswithoutami = np.setdiff1d(snaps,snapIds)
    #snapswithoutami = list(set(SnapshotList).symmetric_difference(set(snapIds)))
    snapswithoutami =  set(SnapshotList) - set(snapIds) 
    list_snapswithoutami = list(snapswithoutami)
    print('Snapshots Without AMI:',snapswithoutami)

    #Exclude AMIs used in Launch Templates
    amiIds =  set(amiIds) - set(excludedAMIIds)

    #Deregisteres AMI's which are associated with provided snapshot id's.
    for amiID in amiIds:
        #print('lc_response:',lc_response)
        print('Deregistering:',amiID)
        client.deregister_image(ImageId=amiID)

    #Removes SnapshotId's which are linked with AMI's
    for snpId in snapIds:
        print('Deleting:',snpId)
        client.delete_snapshot(SnapshotId=str(snpId))

    #Removes SnapshotID's which are not linked with AMI's but provided in Snapshot List.
    for snpIdnotinAmis in snapswithoutami:
        try:
            print('Deleting SnapshotId not in Ami:',snpIdnotinAmis)
            client.delete_snapshot(SnapshotId=str(snpIdnotinAmis))
        except botocore.exceptions.ClientError:
            message = "Snapshot id '{id}' not found"
            print (message.format(id = snpIdnotinAmis))

def get_ami_in_launch_configuration():
    global excludedAMIs
    excludedAMIs = []
    ec2_client = boto3.client('autoscaling') 
    lc_response=ec2_client.describe_launch_configurations()
    print('lc_response:',lc_response)
    for item in lc_response['LaunchConfigurations']:
        print('lc_response[0]:',lc_response['LaunchConfigurations'][0])
        dir(lc_response['LaunchConfigurations'][0])
        ImageId = lc_response['LaunchConfigurations'][0]['ImageId']
        print('Image ID from LaunchConfiguration:',ImageId)
        excludedAMIs.append(ImageId)
    
    return excludedAMIs

def check_if_snapshot_associated_with_launch_configuration(amiparam):
    excludeSnapshot = False
    ec2_client = boto3.client('autoscaling') 
    snap_response=ec2_client.describe_launch_configurations()
    print('snap_response:',snap_response)
    for item in snap_response['LaunchConfigurations']:
        #print('snap_response[0]:',snap_response['LaunchConfigurations'][0])
        dir(snap_response['LaunchConfigurations'][0])
        ImageId = snap_response['LaunchConfigurations'][0]['ImageId']
        #print('Image ID from LaunchConfiguration:',ImageId)
        if ImageId == amiparam:
            excludeSnapshot == True
            print('Image ID to exclude snapshot:', ImageId)
            return
    
    return excludeSnapshot
